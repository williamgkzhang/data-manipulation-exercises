import { PIECES } from './baseFixtures'

export const BASE_ARRAY = [PIECES.Luna, PIECES.Puck, PIECES.Tusk]
export const ADD_ARRAY_RESULT = [...BASE_ARRAY, PIECES.Tidehunter]
