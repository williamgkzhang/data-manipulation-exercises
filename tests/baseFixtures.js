export const PIECES = {
  Luna: {
    name: "Luna",
    species: "Elf",
    class: "Knight",
    cost: 2
  },
  Puck: { 
    name: "Puck",
    species: "Dragon",
    class: "Mage",
    cost: 2
  },
  Tusk: { 
    name: "Tusk",
    species: "Beast",
    class: "Warrior",
    cost: 1
  },
  Tidehunter: {
    name: "Tidehunter",
    species: "Naga",
    class: "Hunter",
    cost: 5
  }
}
